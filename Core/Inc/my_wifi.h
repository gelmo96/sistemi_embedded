/*
 * my_wifi.h
 *
 *  Created on: 10 mar 2021
 *      Author: Fede
 */

#ifndef INC_MY_WIFI_H_
#define INC_MY_WIFI_H_

#include "wifi.h"
#include "main.h"
#include <stdbool.h>

/* Private defines -----------------------------------------------------------*/
#define WIFI_WRITE_TIMEOUT 10000
#define WIFI_READ_TIMEOUT  10000
#define PORT           80
#define MBED_CONF_APP_WIFI_SSID WIFI_SSID
#define WIFI_SSID "Casa"
//#define WIFI_SSID "Mi9T"
#define MBED_CONF_APP_WIFI_PASSWORD WIFI_PWD
#define WIFI_PWD "Assur4ncent0urix"
//#define WIFI_PWD "geppo123"

/* Private typedef------------------------------------------------------------*/
typedef enum
{
  WS_IDLE = 0,
  WS_CONNECTED,
  WS_DISCONNECTED,
  WS_ERROR,
} WebServerState_t;

/* Private macro -------------------------------------------------------------*/
int wifi_sample_run(void);
void WebServerProcess(float temp_val, float hum_val, float pres_val, int16_t accel_val[3],
		int16_t magn_val[3], float gyro_val[3]);
WIFI_Status_t SendWebPage(float temp_val, float hum_val, float pres_val, int16_t accel_val[3],
		int16_t magn_val[3], float gyro_val[3]);
/* Private variables -----------------------------------------------------------*/
char mac_addr[18];
char ip_addr[16];


#endif /* INC_MY_WIFI_H_ */
