/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

#ifdef __GNUC__
/* With GCC, small printf (option LD Linker->Libraries->Small printf
 set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TAG_SENSORI 0x003

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DFSDM_Channel_HandleTypeDef hdfsdm1_channel1;

I2C_HandleTypeDef hi2c2;

QSPI_HandleTypeDef hqspi;

SPI_HandleTypeDef hspi3;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

PCD_HandleTypeDef hpcd_USB_OTG_FS;

osThreadId taskAccelerHandle;
osThreadId taskMagnetHandle;
osThreadId taskHumHandle;
osThreadId taskBottoneHandle;
osThreadId taskTempHandle;
osThreadId taskPressHandle;
osThreadId taskGyroHandle;
osThreadId taskPrintHandle;
osThreadId taskLed1Handle;
osThreadId taskLed2Handle;
osThreadId taskWebServerHandle;
osMessageQId codaLedHandle;
osTimerId timerSensoriHandle;
osMutexId mutex_sensoriHandle;
/* USER CODE BEGIN PV */

// valori letti dai sensori
float temp_val = 0;
float hum_val = 0;
float pres_val = 0;
int16_t accel_val[3] = { 0, 0, 0 };
int16_t magn_val[3] = { 0, 0, 0 };
float gyro_val[3] = { 0, 0, 0 };

//Strutture dati per stampe messaggi di sistema
Data data_print_timer = { SISTEMA, S_TIMER, 0, 0 };
Data data_print_bottone = { SISTEMA, S_BOTTONE, 0, 0 };

// Strutture dati per gestione led
DataLed data_led_1 = { LED_1, 3, 200 };
DataLed data_led_2 = { LED_2, 5, 100 };

//Strutture dati sensori per stampa
Data data_print_temp = { TEMPERATURA, 0, 0, 0 };
Data data_print_hum = { UMIDITA, 0, 0, 0 };
Data data_print_press = { PRESSIONE, 0, 0, 0 };
Data data_print_accel = { ACCELEROMETRO, 0, 0, 0 };
Data data_print_magneto = { MAGNETOMETRO, 0, 0, 0 };
Data data_print_gyro = { GIROSCOPIO, 0, 0, 0 };

char system_strings[][50] = {
		"@@@@@@@ Hai premuto il bottone @@@@@@",
		"####### Lettura dei sensori da timer ######" };
char source_strings[][50] = {
		"\tPressione", "\tTemperatura", "\tUmidita'",
		"\tGiroscopio", "\tMagnetometro", "\tAccelerometro", "\tSistema" };
char wifi_strings[][50] = {
		"> ES-WIFI Initialized",
		"> es-wifi module MAC Address : ", "> ERROR : CANNOT get MAC address",
		"> es-wifi module connected", "> es-wifi module got IP Address : ",
		"> Start HTTP Server...\n\r> Wait for connection...",
		"> ERROR : es-wifi module CANNOT get IP address",
		"> ERROR : es-wifi module NOT connected",
		"> ERROR : WIFI Module cannot be initialized",
		"> ERROR : Connection cannot be established",
		"> ERROR : Cannot send web page", "> HTTP Server Started",
		"> ERROR: Reached state WS_ERROR" };

// Diagnostica per minimizzare lo stack utilizzato
#ifdef STACK_OPT
UBaseType_t wm_temp;
UBaseType_t wm_hum;
UBaseType_t wm_press;
UBaseType_t wm_accel;
UBaseType_t wm_magn;
UBaseType_t wm_gyro;

UBaseType_t wm_led1;
UBaseType_t wm_led2;
UBaseType_t wm_print;

UBaseType_t wm_web;

UBaseType_t wm_bottone;
UBaseType_t wm_timer;

Data data_print_wm = {WATERMARK, 0, 0, 0};

#endif

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DFSDM1_Init(void);
static void MX_I2C2_Init(void);
static void MX_QUADSPI_Init(void);
static void MX_SPI3_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_USB_OTG_FS_PCD_Init(void);
static void MX_USART2_UART_Init(void);
void StartTaskAcceller(void const * argument);
void StartTaskMagnet(void const * argument);
void StartTaskHum(void const * argument);
void StartTaskBottone(void const * argument);
void StartTaskTemp(void const * argument);
void StartTaskPress(void const * argument);
void StartTaskGyro(void const * argument);
void StartTaskPrint(void const * argument);
void StartTaskLed1(void const * argument);
void StartTaskLed2(void const * argument);
void StartTaskWebServer(void const * argument);
void TimerCallback(void const * argument);

/* USER CODE BEGIN PFP */
void SvegliaTutti(void const *argument);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

PUTCHAR_PROTOTYPE {
	/* Place your implementation of fputc here */
	/* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */
	HAL_UART_Transmit(&huart1, (uint8_t*) &ch, 1, 0xFFFF);

	return ch;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DFSDM1_Init();
  MX_I2C2_Init();
  MX_QUADSPI_Init();
  MX_SPI3_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  MX_USB_OTG_FS_PCD_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
	//led verde 2
	BSP_LED_Init(LED_GREEN2);
	//led verde 1
	BSP_LED_Init(LED_GREEN1);
	// bottone user (blu)
	BSP_PB_Init(BUTTON_USER, BUTTON_MODE_GPIO);
	//Init sensore temperatura
	BSP_TSENSOR_Init();
	//Init sensore umidita'
	BSP_HSENSOR_Init();
	//Init sensore pressione
	BSP_PSENSOR_Init();
	//Init sensore accelerometro
	BSP_ACCELERO_Init();
	//Init sensore giroscopio
	BSP_GYRO_Init();
	//Init sensore magnetometro
	BSP_MAGNETO_Init();

  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of mutex_sensori */
  osMutexDef(mutex_sensori);
  mutex_sensoriHandle = osMutexCreate(osMutex(mutex_sensori));

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of timerSensori */
  osTimerDef(timerSensori, TimerCallback);
  timerSensoriHandle = osTimerCreate(osTimer(timerSensori), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
	//timer per sveglia tutti i sensori
	osTimerStart(timerSensoriHandle, 20000);
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of codaLed */
  osMessageQDef(codaLed, 16, DataLed);
  codaLedHandle = osMessageCreate(osMessageQ(codaLed), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
	osMessageQDef(codaPrint, 128, Data);
	codaPrintHandle = osMessageCreate(osMessageQ(codaPrint), NULL);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of taskAcceler */
  osThreadDef(taskAcceler, StartTaskAcceller, osPriorityNormal, 0, 128);
  taskAccelerHandle = osThreadCreate(osThread(taskAcceler), NULL);

  /* definition and creation of taskMagnet */
  osThreadDef(taskMagnet, StartTaskMagnet, osPriorityNormal, 0, 128);
  taskMagnetHandle = osThreadCreate(osThread(taskMagnet), NULL);

  /* definition and creation of taskHum */
  osThreadDef(taskHum, StartTaskHum, osPriorityNormal, 0, 128);
  taskHumHandle = osThreadCreate(osThread(taskHum), NULL);

  /* definition and creation of taskBottone */
  osThreadDef(taskBottone, StartTaskBottone, osPriorityNormal, 0, 64);
  taskBottoneHandle = osThreadCreate(osThread(taskBottone), NULL);

  /* definition and creation of taskTemp */
  osThreadDef(taskTemp, StartTaskTemp, osPriorityNormal, 0, 128);
  taskTempHandle = osThreadCreate(osThread(taskTemp), NULL);

  /* definition and creation of taskPress */
  osThreadDef(taskPress, StartTaskPress, osPriorityNormal, 0, 128);
  taskPressHandle = osThreadCreate(osThread(taskPress), NULL);

  /* definition and creation of taskGyro */
  osThreadDef(taskGyro, StartTaskGyro, osPriorityNormal, 0, 128);
  taskGyroHandle = osThreadCreate(osThread(taskGyro), NULL);

  /* definition and creation of taskPrint */
  osThreadDef(taskPrint, StartTaskPrint, osPriorityAboveNormal, 0, 177);
  taskPrintHandle = osThreadCreate(osThread(taskPrint), NULL);

  /* definition and creation of taskLed1 */
  osThreadDef(taskLed1, StartTaskLed1, osPriorityAboveNormal, 0, 64);
  taskLed1Handle = osThreadCreate(osThread(taskLed1), NULL);

  /* definition and creation of taskLed2 */
  osThreadDef(taskLed2, StartTaskLed2, osPriorityAboveNormal, 0, 64);
  taskLed2Handle = osThreadCreate(osThread(taskLed2), NULL);

  /* definition and creation of taskWebServer */
  osThreadDef(taskWebServer, StartTaskWebServer, osPriorityNormal, 0, 274);
  taskWebServerHandle = osThreadCreate(osThread(taskWebServer), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_I2C2
                              |RCC_PERIPHCLK_DFSDM1|RCC_PERIPHCLK_USB;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInit.Dfsdm1ClockSelection = RCC_DFSDM1CLKSOURCE_PCLK;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable MSI Auto calibration
  */
  HAL_RCCEx_EnableMSIPLLMode();
}

/**
  * @brief DFSDM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_DFSDM1_Init(void)
{

  /* USER CODE BEGIN DFSDM1_Init 0 */

  /* USER CODE END DFSDM1_Init 0 */

  /* USER CODE BEGIN DFSDM1_Init 1 */

  /* USER CODE END DFSDM1_Init 1 */
  hdfsdm1_channel1.Instance = DFSDM1_Channel1;
  hdfsdm1_channel1.Init.OutputClock.Activation = ENABLE;
  hdfsdm1_channel1.Init.OutputClock.Selection = DFSDM_CHANNEL_OUTPUT_CLOCK_SYSTEM;
  hdfsdm1_channel1.Init.OutputClock.Divider = 2;
  hdfsdm1_channel1.Init.Input.Multiplexer = DFSDM_CHANNEL_EXTERNAL_INPUTS;
  hdfsdm1_channel1.Init.Input.DataPacking = DFSDM_CHANNEL_STANDARD_MODE;
  hdfsdm1_channel1.Init.Input.Pins = DFSDM_CHANNEL_FOLLOWING_CHANNEL_PINS;
  hdfsdm1_channel1.Init.SerialInterface.Type = DFSDM_CHANNEL_SPI_RISING;
  hdfsdm1_channel1.Init.SerialInterface.SpiClock = DFSDM_CHANNEL_SPI_CLOCK_INTERNAL;
  hdfsdm1_channel1.Init.Awd.FilterOrder = DFSDM_CHANNEL_FASTSINC_ORDER;
  hdfsdm1_channel1.Init.Awd.Oversampling = 1;
  hdfsdm1_channel1.Init.Offset = 0;
  hdfsdm1_channel1.Init.RightBitShift = 0x00;
  if (HAL_DFSDM_ChannelInit(&hdfsdm1_channel1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DFSDM1_Init 2 */

  /* USER CODE END DFSDM1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x10909CEC;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief QUADSPI Initialization Function
  * @param None
  * @retval None
  */
static void MX_QUADSPI_Init(void)
{

  /* USER CODE BEGIN QUADSPI_Init 0 */

  /* USER CODE END QUADSPI_Init 0 */

  /* USER CODE BEGIN QUADSPI_Init 1 */

  /* USER CODE END QUADSPI_Init 1 */
  /* QUADSPI parameter configuration*/
  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 2;
  hqspi.Init.FifoThreshold = 4;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_HALFCYCLE;
  hqspi.Init.FlashSize = 23;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN QUADSPI_Init 2 */

  /* USER CODE END QUADSPI_Init 2 */

}

/**
  * @brief SPI3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI3_Init(void)
{

  /* USER CODE BEGIN SPI3_Init 0 */

  /* USER CODE END SPI3_Init 0 */

  /* USER CODE BEGIN SPI3_Init 1 */

  /* USER CODE END SPI3_Init 1 */
  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 7;
  hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi3.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI3_Init 2 */

  /* USER CODE END SPI3_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief USB_OTG_FS Initialization Function
  * @param None
  * @retval None
  */
static void MX_USB_OTG_FS_PCD_Init(void)
{

  /* USER CODE BEGIN USB_OTG_FS_Init 0 */

  /* USER CODE END USB_OTG_FS_Init 0 */

  /* USER CODE BEGIN USB_OTG_FS_Init 1 */

  /* USER CODE END USB_OTG_FS_Init 1 */
  hpcd_USB_OTG_FS.Instance = USB_OTG_FS;
  hpcd_USB_OTG_FS.Init.dev_endpoints = 6;
  hpcd_USB_OTG_FS.Init.speed = PCD_SPEED_FULL;
  hpcd_USB_OTG_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
  hpcd_USB_OTG_FS.Init.Sof_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.low_power_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.lpm_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.battery_charging_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.use_dedicated_ep1 = DISABLE;
  hpcd_USB_OTG_FS.Init.vbus_sensing_enable = DISABLE;
  if (HAL_PCD_Init(&hpcd_USB_OTG_FS) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USB_OTG_FS_Init 2 */

  /* USER CODE END USB_OTG_FS_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, M24SR64_Y_RF_DISABLE_Pin|M24SR64_Y_GPO_Pin|ISM43362_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3|LED1_Pin|SPBTLE_RF_RST_Pin|ARD_D9_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, ARD_D8_Pin|ISM43362_BOOT0_Pin|ISM43362_WAKEUP_Pin|LED2_Pin
                          |SPSGRF_915_SDN_Pin|ARD_D5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, USB_OTG_FS_PWR_EN_Pin|PMOD_RESET_Pin|STSAFE_A100_RESET_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SPBTLE_RF_SPI3_CSN_GPIO_Port, SPBTLE_RF_SPI3_CSN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, VL53L0X_XSHUT_Pin|LED3_WIFI__LED4_BLE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SPSGRF_915_SPI3_CSN_GPIO_Port, SPSGRF_915_SPI3_CSN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ISM43362_SPI3_CSN_GPIO_Port, ISM43362_SPI3_CSN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : M24SR64_Y_RF_DISABLE_Pin M24SR64_Y_GPO_Pin ISM43362_RST_Pin ISM43362_SPI3_CSN_Pin */
  GPIO_InitStruct.Pin = M24SR64_Y_RF_DISABLE_Pin|M24SR64_Y_GPO_Pin|ISM43362_RST_Pin|ISM43362_SPI3_CSN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : USB_OTG_FS_OVRCR_EXTI3_Pin SPSGRF_915_GPIO3_EXTI5_Pin SPBTLE_RF_IRQ_EXTI6_Pin ISM43362_DRDY_EXTI1_Pin */
  GPIO_InitStruct.Pin = USB_OTG_FS_OVRCR_EXTI3_Pin|SPSGRF_915_GPIO3_EXTI5_Pin|SPBTLE_RF_IRQ_EXTI6_Pin|ISM43362_DRDY_EXTI1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : BOTTONE_BLU_Pin VL53L0X_GPIO1_EXTI7_Pin LSM3MDL_DRDY_EXTI8_Pin */
  GPIO_InitStruct.Pin = BOTTONE_BLU_Pin|VL53L0X_GPIO1_EXTI7_Pin|LSM3MDL_DRDY_EXTI8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA3 LED1_Pin SPBTLE_RF_RST_Pin ARD_D9_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_3|LED1_Pin|SPBTLE_RF_RST_Pin|ARD_D9_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : ARD_D8_Pin ISM43362_BOOT0_Pin ISM43362_WAKEUP_Pin LED2_Pin
                           SPSGRF_915_SDN_Pin ARD_D5_Pin SPSGRF_915_SPI3_CSN_Pin */
  GPIO_InitStruct.Pin = ARD_D8_Pin|ISM43362_BOOT0_Pin|ISM43362_WAKEUP_Pin|LED2_Pin
                          |SPSGRF_915_SDN_Pin|ARD_D5_Pin|SPSGRF_915_SPI3_CSN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LPS22HB_INT_DRDY_EXTI0_Pin LSM6DSL_INT1_EXTI11_Pin ARD_D2_Pin HTS221_DRDY_EXTI15_Pin
                           PMOD_IRQ_EXTI12_Pin */
  GPIO_InitStruct.Pin = LPS22HB_INT_DRDY_EXTI0_Pin|LSM6DSL_INT1_EXTI11_Pin|ARD_D2_Pin|HTS221_DRDY_EXTI15_Pin
                          |PMOD_IRQ_EXTI12_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : USB_OTG_FS_PWR_EN_Pin SPBTLE_RF_SPI3_CSN_Pin PMOD_RESET_Pin STSAFE_A100_RESET_Pin */
  GPIO_InitStruct.Pin = USB_OTG_FS_PWR_EN_Pin|SPBTLE_RF_SPI3_CSN_Pin|PMOD_RESET_Pin|STSAFE_A100_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : VL53L0X_XSHUT_Pin LED3_WIFI__LED4_BLE_Pin */
  GPIO_InitStruct.Pin = VL53L0X_XSHUT_Pin|LED3_WIFI__LED4_BLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
/* SvegliaTutti function */
void SvegliaTutti(void const *argument) {

	osSignalSet(taskPressHandle, TAG_SENSORI);
	osSignalSet(taskAccelerHandle, TAG_SENSORI);
	osSignalSet(taskGyroHandle, TAG_SENSORI);
	osSignalSet(taskHumHandle, TAG_SENSORI);
	osSignalSet(taskMagnetHandle, TAG_SENSORI);
	osSignalSet(taskTempHandle, TAG_SENSORI);

}

//callback bottone user (blu)
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	//segnala solo se l'interrupt e` scatenato dal bottone utente
	if (USER_BUTTON_PIN == GPIO_Pin) {
		osSignalSet(taskBottoneHandle, BUTTON_USER);
	}

}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartTaskAcceller */
/**
 * @brief  Function implementing the taskAceller thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskAcceller */
void StartTaskAcceller(void const * argument)
{
  /* USER CODE BEGIN 5 */
	/* Infinite loop */
	for (;;) {
		osSignalWait(TAG_SENSORI, osWaitForever);

		osMutexWait(mutex_sensoriHandle, osWaitForever);
		BSP_ACCELERO_AccGetXYZ(accel_val);
		osMutexRelease(mutex_sensoriHandle);

		data_print_accel.value_x = accel_val[0];
		data_print_accel.value_y = accel_val[1];
		data_print_accel.value_z = accel_val[2];

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_accel,
		osWaitForever);

#ifdef STACK_OPT
		wm_accel = uxTaskGetStackHighWaterMark( NULL);
#endif

	}
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartTaskMagnet */
/**
 * @brief Function implementing the taskMagnet thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskMagnet */
void StartTaskMagnet(void const * argument)
{
  /* USER CODE BEGIN StartTaskMagnet */
	/* Infinite loop */
	for (;;) {
		osSignalWait(TAG_SENSORI, osWaitForever);

		osMutexWait(mutex_sensoriHandle, osWaitForever);
		BSP_MAGNETO_GetXYZ(magn_val);
		osMutexRelease(mutex_sensoriHandle);

		data_print_magneto.value_x = magn_val[0];
		data_print_magneto.value_y = magn_val[1];
		data_print_magneto.value_z = magn_val[2];

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_magneto,
		osWaitForever);

#ifdef STACK_OPT
		wm_magn = uxTaskGetStackHighWaterMark( NULL);
#endif
	}
  /* USER CODE END StartTaskMagnet */
}

/* USER CODE BEGIN Header_StartTaskHum */
/**
 * @brief Function implementing the taskHum thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskHum */
void StartTaskHum(void const * argument)
{
  /* USER CODE BEGIN StartTaskHum */
	/* Infinite loop */
	for (;;) {
		osSignalWait(TAG_SENSORI, osWaitForever);

		osMutexWait(mutex_sensoriHandle, osWaitForever);
		hum_val = (float) BSP_HSENSOR_ReadHumidity();
		osMutexRelease(mutex_sensoriHandle);

		data_print_hum.value_x = hum_val;

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_hum,
		osWaitForever);

#ifdef STACK_OPT
		wm_hum = uxTaskGetStackHighWaterMark( NULL);
#endif
	}
  /* USER CODE END StartTaskHum */
}

/* USER CODE BEGIN Header_StartTaskBottone */
/**
 * @brief Function implementing the taskBottone thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskBottone */
void StartTaskBottone(void const * argument)
{
  /* USER CODE BEGIN StartTaskBottone */
	/* Infinite loop */
	for (;;) {
		osSignalWait(BUTTON_USER, osWaitForever);

		osMessagePut(codaLedHandle, (uint32_t) &data_led_1, osWaitForever);

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_bottone,
		osWaitForever);

		SvegliaTutti(NULL);

#ifdef STACK_OPT
		wm_bottone = uxTaskGetStackHighWaterMark( NULL);
		osMessagePut(codaPrintHandle, (uint32_t) &data_print_wm, osWaitForever);
		#endif

	}
  /* USER CODE END StartTaskBottone */
}

/* USER CODE BEGIN Header_StartTaskTemp */
/**
 * @brief Function implementing the taskTemp thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskTemp */
void StartTaskTemp(void const * argument)
{
  /* USER CODE BEGIN StartTaskTemp */
	/* Infinite loop */
	for (;;) {
		osSignalWait(TAG_SENSORI, osWaitForever);

		osMutexWait(mutex_sensoriHandle, osWaitForever);
		temp_val = (float) BSP_TSENSOR_ReadTemp();
		osMutexRelease(mutex_sensoriHandle);

		data_print_temp.value_x = temp_val;

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_temp,
		osWaitForever);

#ifdef STACK_OPT
		wm_temp = uxTaskGetStackHighWaterMark( NULL);
#endif
	}
  /* USER CODE END StartTaskTemp */
}

/* USER CODE BEGIN Header_StartTaskPress */
/**
 * @brief Function implementing the taskPress thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskPress */
void StartTaskPress(void const * argument)
{
  /* USER CODE BEGIN StartTaskPress */
	/* Infinite loop */
	for (;;) {
		osSignalWait(TAG_SENSORI, osWaitForever);

		osMutexWait(mutex_sensoriHandle, osWaitForever);
		pres_val = (float) BSP_PSENSOR_ReadPressure();
		osMutexRelease(mutex_sensoriHandle);

		data_print_press.value_x = pres_val;

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_press,
		osWaitForever);

#ifdef STACK_OPT
		wm_press = uxTaskGetStackHighWaterMark( NULL);
		#endif
	}
  /* USER CODE END StartTaskPress */
}

/* USER CODE BEGIN Header_StartTaskGyro */
/**
 * @brief Function implementing the taskGyro thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskGyro */
void StartTaskGyro(void const * argument)
{
  /* USER CODE BEGIN StartTaskGyro */
	/* Infinite loop */
	for (;;) {
		osSignalWait(TAG_SENSORI, osWaitForever);

		osMutexWait(mutex_sensoriHandle, osWaitForever);
		BSP_GYRO_GetXYZ(gyro_val);
		osMutexRelease(mutex_sensoriHandle);

		data_print_gyro.value_x = gyro_val[0];
		data_print_gyro.value_y = gyro_val[1];
		data_print_gyro.value_z = gyro_val[2];

		osMessagePut(codaPrintHandle, (uint32_t) &data_print_gyro,
		osWaitForever);

#ifdef STACK_OPT
		wm_gyro = uxTaskGetStackHighWaterMark( NULL);
		#endif
	}
  /* USER CODE END StartTaskGyro */
}

/* USER CODE BEGIN Header_StartTaskPrint */
/**
 * @brief Function implementing the taskPrint thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskPrint */
void StartTaskPrint(void const * argument)
{
  /* USER CODE BEGIN StartTaskPrint */
	/* Infinite loop */

	for (;;) {

		osEvent ret_value = osMessageGet(codaPrintHandle, osWaitForever);
		uint8_t source = ((Data*) ret_value.value.p)->source;

		//tre valori
		if (source == GIROSCOPIO || source == MAGNETOMETRO
				|| source == ACCELEROMETRO) {

			printf("%s = %.2f %.2f %.2f\n\r",
					source_strings[((Data*) ret_value.value.p)->source],
					((Data*) ret_value.value.p)->value_x,
					((Data*) ret_value.value.p)->value_y,
					((Data*) ret_value.value.p)->value_z);

			//una stringa, messaggio di sistema
		} else if (source == SISTEMA) {
			printf("%s\n\r",
					system_strings[(int) ((Data*) ret_value.value.p)->value_x]);

			//un solo valore float
		} else if (source == WIFI) {
			int string_index = ((Data*) ret_value.value.p)->value_x;

			//stringa MAC addr
			if (string_index == 1) {
				printf("%s %s\n\r", wifi_strings[string_index], mac_addr);
				//string IP addr
			} else if (string_index == 4) {
				printf("%s %s\n\r", wifi_strings[string_index], ip_addr);
				//all other strings
			} else {
				printf("%s\n\r", wifi_strings[string_index]);
			}

#ifdef STACK_OPT
		} else if (source == WATERMARK){
			printf("HW gyroscopio: %d\n\r", (int)wm_gyro);
			printf("HW pressione: %d\n\r", (int)wm_press);
			printf("HW temperatura: %d\n\r", (int)wm_temp);
			printf("HW bottone: %d\n\r", (int)wm_bottone);
			printf("HW humidita: %d\n\r", (int)wm_hum);
			printf("HW magnetometro: %d\n\r", (int)wm_magn);
			printf("HW accelerometro: %d\n\r", (int)wm_accel);
			printf("HW print: %d\n\r", (int)wm_print);
			printf("HW led1: %d\n\r", (int)wm_led1);
			printf("HW led2: %d\n\r", (int)wm_led2);
			printf("HW web: %d\n\r", (int)wm_web);
			printf("HW timer: %d\n\r", (int)wm_timer);
#endif

		} else {
			printf("%s = %.2f\n\r",
					source_strings[((Data*) ret_value.value.p)->source],
					((Data*) ret_value.value.p)->value_x);
		}

		fflush(stdout);

#ifdef STACK_OPT
		wm_print = uxTaskGetStackHighWaterMark( NULL);
		#endif

	}

  /* USER CODE END StartTaskPrint */
}

/* USER CODE BEGIN Header_StartTaskLed1 */
/**
 * @brief Function implementing the taskLed1 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskLed1 */
void StartTaskLed1(void const * argument)
{
  /* USER CODE BEGIN StartTaskLed1 */
	/* Infinite loop */
	for (;;) {
		osEvent ret_value = osMessagePeek(codaLedHandle, osWaitForever);
		uint8_t ledId = ((DataLed*) ret_value.value.p)->ledId;

		if (ledId == LED_1) {
			ret_value = osMessageGet(codaLedHandle, osWaitForever);

			uint8_t nLampeggi = ((DataLed*) ret_value.value.p)->nLampeggi;
			uint16_t msPausa = ((DataLed*) ret_value.value.p)->msPausa;

			for (int i = 0; i < nLampeggi; ++i) {
				BSP_LED_On(LED_GREEN1);
				osDelay(msPausa);
				BSP_LED_Off(LED_GREEN1);
				osDelay(msPausa);
			}
		}

#ifdef STACK_OPT
		wm_led1 = uxTaskGetStackHighWaterMark( NULL);
		#endif
	}
  /* USER CODE END StartTaskLed1 */
}

/* USER CODE BEGIN Header_StartTaskLed2 */
/**
 * @brief Function implementing the taskLed2 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskLed2 */
void StartTaskLed2(void const * argument)
{
  /* USER CODE BEGIN StartTaskLed2 */
	/* Infinite loop */
	for (;;) {
		osEvent ret_value = osMessagePeek(codaLedHandle, osWaitForever);
		uint8_t ledId = ((DataLed*) ret_value.value.p)->ledId;

		if (ledId == LED_2) {
			ret_value = osMessageGet(codaLedHandle, osWaitForever);

			uint8_t nLampeggi = ((DataLed*) ret_value.value.p)->nLampeggi;
			uint16_t msPausa = ((DataLed*) ret_value.value.p)->msPausa;

			for (int i = 0; i < nLampeggi; ++i) {
				BSP_LED_On(LED_GREEN2);
				osDelay(msPausa);
				BSP_LED_Off(LED_GREEN2);
				osDelay(msPausa);
			}
		}

#ifdef STACK_OPT
		wm_led2 = uxTaskGetStackHighWaterMark( NULL);
		#endif
	}

  /* USER CODE END StartTaskLed2 */
}

/* USER CODE BEGIN Header_StartTaskWebServer */
/**
 * @brief Function implementing the taskWebServer thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartTaskWebServer */
void StartTaskWebServer(void const * argument)
{
  /* USER CODE BEGIN StartTaskWebServer */

	/* Una lettura per non inviare dati = 0*/
	SvegliaTutti(NULL);

	if (wifi_sample_run() != 0) {
		return;
	}
	/* Infinite loop */
	for (;;) {
		WebServerProcess(temp_val, hum_val, pres_val, accel_val, magn_val,
				gyro_val);

#ifdef STACK_OPT
		wm_web = uxTaskGetStackHighWaterMark( NULL);
		#endif
	}
  /* USER CODE END StartTaskWebServer */
}

/* TimerCallback function */
void TimerCallback(void const * argument)
{
  /* USER CODE BEGIN TimerCallback */

	osMessagePut(codaLedHandle, (uint32_t) &data_led_2, osWaitForever);

	osMessagePut(codaPrintHandle, (uint32_t) &data_print_timer, osWaitForever);

	SvegliaTutti(NULL);

#ifdef STACK_OPT
		wm_timer = uxTaskGetStackHighWaterMark( NULL);
		#endif
  /* USER CODE END TimerCallback */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
