/*
 * my_wifi.c
 *
 *  Created on: 10 mar 2021
 *      Author: Fede
 */

#include "my_wifi.h"


/* Private variables ---------------------------------------------------------*/
//Serial pc(SERIAL_TX, SERIAL_RX);
static uint8_t http[1024];
static uint8_t resp[1024];
uint16_t respLen;
uint8_t IP_Addr[4];
uint8_t MAC_Addr[6];
int32_t Socket = -1;
static WebServerState_t State = WS_ERROR;
char ModuleName[32];
int iter = 0;
bool do_once = true;

int wifi_sample_run(void) {

	/*Initialize and use WIFI module */
	if (WIFI_Init() == WIFI_STATUS_OK) {
		if(do_once){
			Data data_to_send = { WIFI, 0, 0, 0};
			osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
		}

		if (WIFI_GetMAC_Address(MAC_Addr) == WIFI_STATUS_OK) {
			if(do_once){
				sprintf(mac_addr, "%X:%X:%X:%X:%X:%X", MAC_Addr[0], MAC_Addr[1], MAC_Addr[2], MAC_Addr[3],
						MAC_Addr[4], MAC_Addr[5]);
				Data data_to_send = { WIFI, 1, 0, 0};
				osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
			}

		} else {
			if(do_once){
				Data data_to_send = { WIFI, 2, 0, 0};
				osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
			}
		}

		if (WIFI_Connect(MBED_CONF_APP_WIFI_SSID, MBED_CONF_APP_WIFI_PASSWORD,
				WIFI_ECN_WPA2_PSK) == WIFI_STATUS_OK) {
			if(do_once){
				Data data_to_send = { WIFI, 3, 0, 0};
				osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
			}

			if (WIFI_GetIP_Address(IP_Addr) == WIFI_STATUS_OK) {
				if(do_once){
					sprintf(ip_addr, "%d.%d.%d.%d", IP_Addr[0], IP_Addr[1], IP_Addr[2], IP_Addr[3]);
					Data data_to_send = { WIFI, 4, 0, 0};
					osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);

					Data data_to_send_2 = { WIFI, 5, 0, 0};
					osMessagePut(codaPrintHandle, (uint32_t) &data_to_send_2, osWaitForever);
				}
				State = WS_IDLE;
			} else {

				Data data_to_send = { WIFI, 6, 0, 0};
				osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);

				return -1;
			}
		} else {

			Data data_to_send = { WIFI, 7, 0, 0};
			osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);

			return -1;
		}
	} else {

		Data data_to_send = { WIFI, 8, 0, 0};
		osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);

		return -1;
	}

	iter = 0;
	return 0;
}

/**
 * @brief  Send HTML page
 * @param  None
 * @retval None
 */
void WebServerProcess(float temp_val, float hum_val, float pres_val,
		int16_t accel_val[3], int16_t magn_val[3], float gyro_val[3]) {

	switch (State) {
		case WS_IDLE:;

			char rev[100];
			WIFI_GetModuleFwRevision(rev);
			//printf("%s\n\r", rev);

			Socket = 0;
			WIFI_StartServer(Socket, WIFI_TCP_PROTOCOL, "", PORT);

			if (Socket != -1) {
				if(do_once){
					Data data_to_send = { WIFI, 11, 0, 0};
					osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
				}
				State = WS_CONNECTED;
				do_once = false;
			} else {
				Data data_to_send = { WIFI, 9, 0, 0};
				osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
				State = WS_ERROR;
			}
			break;

		case WS_CONNECTED:

			WIFI_ReceiveData(Socket, resp, 1200, &respLen, WIFI_READ_TIMEOUT);

			if (respLen > 0) {

				iter += 1;

				if(iter > 2){
					WIFI_StopServer(Socket);
					wifi_sample_run();
					break;
				}


				if (strstr((char*) resp, "GET")) /* GET: put web page */
				{
					if (SendWebPage(temp_val, hum_val, pres_val, accel_val, magn_val, gyro_val) != WIFI_STATUS_OK) {
						Data data_to_send = { WIFI, 10, 0, 0};
						osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
						State = WS_ERROR;
					}


				} else if (strstr((char*) resp, "POST"))/* POST: received info */
				{
					if (strstr((char*) resp, "radio")) {
						if (strstr((char*) resp, "radio=0")) {
							//LedState = 0;
							//led = 0;
							//TODO accendi spegni led
						} else if (strstr((char*) resp, "radio=1")) {
							//LedState = 1;
							//led = 1;
							//TODO accendi spegni led
						}

						if (SendWebPage(temp_val, hum_val, pres_val, accel_val,
								magn_val, gyro_val) != WIFI_STATUS_OK) {
							Data data_to_send = { WIFI, 10, 0, 0};
							osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
							State = WS_ERROR;
						}
					}
				}
			}
			if (WIFI_StopServer(Socket) == WIFI_STATUS_OK) {
				WIFI_StartServer(Socket, WIFI_TCP_PROTOCOL, "", PORT);
			} else {
				State = WS_ERROR;
			}
			break;
		case WS_ERROR:;
			Data data_to_send = { WIFI, 12, 0, 0};
			osMessagePut(codaPrintHandle, (uint32_t) &data_to_send, osWaitForever);
			break;

		default:
			break;
		}

}

/**
 * @brief  Send HTML page
 * @param  None
 * @retval None
 */
WIFI_Status_t SendWebPage(float temp_val, float hum_val, float pres_val,
		int16_t accel_val[3], int16_t magn_val[3], float gyro_val[3]) {
	uint8_t temp[50];
	uint16_t SentDataLength;
	WIFI_Status_t ret;

	/* construct web page content */
	strcpy((char*) http,
			(char*) "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\nPragma: no-cache\r\n\r\n");
	strcat((char*) http, (char*) "<!DOCTYPE html><html>\r\n");
	strcat((char*) http,
			(char*) "<head><title>STM32 Web Server</title></head>\r\n<body>\r\n");

	strcat((char*) http, (char*) "<p>{'temperatura':");
	sprintf((char*) temp, "%f", temp_val);
	strcat((char*) http, (char*) temp);
	strcat((char*) http, (char*) ",\r\n");

	strcat((char*) http, (char*) "'umidita':");
	sprintf((char*) temp, "%f", hum_val);
	strcat((char*) http, (char*) temp);
	strcat((char*) http, (char*) ",\r\n");

	strcat((char*) http, (char*) "'pressione':");
	sprintf((char*) temp, "%f", pres_val);
	strcat((char*) http, (char*) temp);
	strcat((char*) http, (char*) ",\r\n");

	strcat((char*) http, (char*) "'accelerometro':");
	sprintf((char*) temp, "{'x':%.2f, 'y':%.2f, 'z':%.2f}", (float)accel_val[0],
			(float)accel_val[1], (float)accel_val[2]);
	strcat((char*) http, (char*) temp);
	strcat((char*) http, (char*) ",\r\n");

	strcat((char*) http, (char*) "'magnetometro':");
	sprintf((char*) temp, "{'x':%.2f, 'y':%.2f, 'z':%.2f}", (float)magn_val[0],
			(float)magn_val[1], (float)magn_val[2]);
	strcat((char*) http, (char*) temp);
	strcat((char*) http, (char*) ",\r\n");

	strcat((char*) http, (char*) "'giroscopio':");
	sprintf((char*) temp, "{'x':%.2f, 'y':%.2f, 'z':%.2f}", (float)gyro_val[0],
			(float)gyro_val[1], (float)gyro_val[2]);
	strcat((char*) http, (char*) temp);
	strcat((char*) http, (char*) "}</p>\r\n");

	strcat((char*) http, (char*) "</body>\r\n</html>\r\n");

	ret = WIFI_SendData(0, (uint8_t*) http, strlen((char*) http),
			&SentDataLength, WIFI_WRITE_TIMEOUT);

	if ((ret == WIFI_STATUS_OK) && (SentDataLength != strlen((char*) http))) {
		ret = WIFI_STATUS_ERROR;
	}

	return ret;
}
